**Mosaicist Pool Tile Art**

Mosaicist is among the few companies that are able to offer a complete mosaic project, including design, manufacture, preparation, and installation. Custom Designing by our artists and designers will make your projects one of a kind. Commissions of old world designs to contemporary aquatic motifs are presented before execution. At Mosaicist, our designs and color combinations are created to enhance the beauty of mosaic art to any application. Must of our selections are custom and personalized. Our products install flush and have a smooth flat finish.

*See our website @ [https://mosaicist.com/](https://mosaicist.com/)*

---

